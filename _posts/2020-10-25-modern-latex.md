---
layout: post
title: "Modern LaTeX"
---

It's been a while. Life has been... interesting the past few years.
I hope you've been okay.

I'm getting back to writing about all this crazy software stuff---more to follow soon.
In the meantime, I also wrote a short introduction to {% include latex.html %}!
Forty years after its inception, it's still [an amazing piece of software](/tex.html)
for creating beautiful writing.
But you shouldn't have to learn a bunch of arcane 80s junk
from a 300 page doorstop to get started.

Check out [<i>Modern {% include latex.html %}</i>](https://assets.bitbashing.io/modern-latex.pdf),
available here for free, or at-cost in dead tree at  
<https://app.thebookpatch.com/BookStore/modern-latex/37e37107-c04d-4278-9d1b-32979616dc85>  
<https://www.amazon.com/Modern-LaTeX-Matt-Kline/dp/B0B7PZB2YB>

