---
layout: post
title: "What every systems programmer should know about concurrency"
redirect_from:
 - lockless-concurrency.html
---

At the start of November, I published
[_What every systems programmer should know about concurrency_](https://assets.bitbashing.io/papers/concurrency-primer.pdf),
a short primer on the topic.
Many thanks to Anthony Williams, Fedor Pikus, Paul McKenney,
[and all of you](https://www.reddit.com/r/programming/comments/7a4uu4/what_every_systems_programmer_should_know_about/) for your fantastic feedback!

<!-- I'm amazed at how widely it was received - to date it has over 240,000 downloads!  -->

In light of it, I'm happy to announce the paper's first major update.
It adds two sections:
one discusses the differences between blocking and lockless algorithms,
and the other explains why `volatile`{:.language-cpp} is not the tool you're
looking for when writing concurrent code.
At some point in the near future,
I also hope to add a section about [false sharing](https://en.wikipedia.org/wiki/False_sharing).

You can always find the latest release at the same URL,
<https://assets.bitbashing.io/papers/concurrency-primer.pdf>,
or on its [Github](https://github.com/mrkline/concurrency-primer)
and [Gitlab](https://gitlab.com/mrkline/concurrency-primer) project pages.

