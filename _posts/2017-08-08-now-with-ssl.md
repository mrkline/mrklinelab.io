---
layout: post
title: "Now with SSL"
---

Good news, everyone! My incoherent screeds are now available with a cute green
lock in the corner of your browser.
Take full confidence that the rambling nonsense you see here is all my own,
and has made it to your screen unmolested by nefarious hackers.

I've been meaning to get HTTPS up on my site for quite a while,
but Github Pages, despite
[people complaining about it since 2014](https://github.com/isaacs/github/issues/156),
doesn't support it for custom domains.
I could drop my site onto AWS or something,
but that would go against my efforts to be as lazy as possible
when it comes to this site's infrastructure.
(See: Jekyll, Disqus, etc.)
After voicing these woes on Reddit in response to
<https://doesmysiteneedhttps.com>,
some kind folks suggested I move my site to Gitlab,
which supports the same "push Markdown to a Git repo" workflow.

So I've done just that, using [Let's Encrypt](https://letsencrypt.org)
to get HTTPS up and running.
A few notes for anybody who is looking to migrate their page from Github
in a similar fashion:

1. Unlike Github (which automatically kicks Jekyll whenever you push commits)
   GitLab requires you to start the static site generation as a "CI" step.
   This just involves adding a YAML file to your repo---the official example
   for Jekyll is [here](https://gitlab.com/pages/jekyll/blob/master/.gitlab-ci.yml).

2. Note that the reference Jekyll setup makes use of Bundler,
   so you'll want to set up your repo with a `Gemfile` and `Gemfile.lock`
   if you haven't done so already.

3. I followed the tutorial at
   <https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt>
   to wire up Let's Encrypt.
   Things were _mostly_ straightforward, but I ran into trouble
   placing the challenge data at the given URL.
   I eventually bumped into <https://stackoverflow.com/a/44296500>,
   which made everything work.
   (Apparently my "problem" was having `permalink` set to `none` in
   `_config.yaml`, which caused an `.html` extension to be appended to the needed URL.)

4. My previous Disqus setup was apparently using the URL of each page
   (including the `http://`) as the identifier for that page's comments thread.
   The good news is that at some point between when I set it up in 2014 and now,
   Disqus introduced [a way](https://help.disqus.com/customer/en/portal/articles/2158629-use-configuration-variables-to-avoid-split-threads-and-missing-comments)
   to specify the canonical URL and a unique ID for each page.
   The bad news is I've set these to the pages' `https://` URLs and their
   relative paths, respectively,
   which means that all comments before now have disappeared.
   Oh well.

<!--
Sorry once again to anybody who actually reads this with any regularity -
I've been dealing with a lot in my personal life for the past few months.
Hopefully I'll be back on the regular grind soon. Actual content to follow.
-->

