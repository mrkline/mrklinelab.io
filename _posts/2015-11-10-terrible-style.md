---
layout: post
title: How to write (and use) a terrible style guide
---

- Focus on things like tab widths and indentation.
  These are the most important rules to guarantee solid, maintainable code.\*

- Be sure not to review and update your style guide.
  Good advice is timeless,
  and tools and techniques rarely change in the software industry.

- Choose a subset of a language based on the restrictions past products faced.
  It is better that all of your projects use outdated advice to ensure consistent
  style than for new projects to use current, better advice.

- Choose a subset of a language to avoid parts newcomers might find
  confusing. After all,
  - Nobody you hire actually knows what they're doing.

  - People spend more time learning a new language or tool than they do actually
    using it to write software.

  - You certainly won't need any advanced or rarely-used language features
    for your project.

- Have hundreds of rules. It is more important to be a language lawyer
  and cover every single edge case than it is to provide a few sane,
  easily-understood guidelines.

- If your guide has hundreds of rules, be sure not to have any tooling or static
  analysis to help enforce them. After all, developers need more bits
  rattling around in their heads as they work.

- Be sure to blindly prescribe rules without providing their rationale,
  counterexamples, or alternatives.

- Be sure not to get any sort of buy-in from your peers.
  If they view style guidelines as those annoying things that collect dust
  on an internal wiki, they will use them less often,
  saving time for everyone involved.

- Be sure to enforce the exact same guide for every project.
  All programs have the same end goals and constraints,
  regardless of their problem domain.

- Be sure to lift your style guide from `$(BIG_COMPANY)`
  Their situation must be identical to yours and their guide must contain only
  good ideas.
  After all, they're `$(BIG_COMPANY)` who made `$(FAMOUS_PRODUCT)`.

---

Of course, these are all terrible ideas.
Instead, focus style guides on overarching concepts
and expand on those with a handful of specific suggestions.
Keep them up to date with current advice. Don't lock yourself in the past.
And while there are certainly extremes, developers spend much more time _using_
tools than they do learning them. Unless your organization is a revolving
door for developers, it doesn't make sense to hobble yourself with some
subset designed for befuddled new hires.

Good style is an interactive, ongoing effort.
People often underestimate how vital it is, but it's a large part of what
separates a clean design from an unmaintainable mess.
The good news is that good engineers care about style automatically,
and a guide just nudges them in the right direction.
The bad news is that some guide on your wiki won't save you from
developers who don't care in the first place.
More generally, processes and formalities can't save you from crappy, apathetic
work.

---

\* Consistent formatting is good for developer sanity,
   but good code runs much deeper.
