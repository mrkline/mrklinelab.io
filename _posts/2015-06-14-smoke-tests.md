---
layout: post
title: In defense of smoke tests
---

In case you missed it, last week the author of the OSX package manager
[Homebrew](http://brew.sh/)
[vented his frustration](https://twitter.com/mxcl/status/608682016205344768)
with his interview experience with Google,
lighting up this month's round of Reddit and Hacker News discussion on how much
programming interviews suck.
And man, can they suck --- anybody with a few years in the industry has
a horror story of some interview from hell.
But what was striking was how many claimed that basic,
"homework" style questions about data structures and algorithms had no place
in a professional programming interview.
I don't know nearly enough details to make claims about Google's hiring practices,
Max Howell's experience with them,
or the question he was asked and how the interviewer asked it,
but dismissing "beginner" questions as inappropriate and stupid is missing a point.

## Why we ~~fight~~ interview

It's important to remember what our goals are in a job interview.
It's an opportunity for both parties to establish a couple of things:

1. **Good fit:** The employer tries to establish that the job candidate
   is someone they would feel comfortable working with.
   The candidate tries to make sure their future coworkers aren't assholes
   or drowning in red tape endlessly streaming from on high.

2. **Competency:** 
   The employer doesn't need to exclusively hire 10x® Rockstars™,
   but they want to make sure the candidate is smart and gets things done.
   The candidate looks for knowledgeable coworkers who aren't grossly mismanaged.

The interview is *not* about:

- Busting the candidate on minutiae of some library
  that any sane person would just look up

- "Puzzler" questions and brainteasers:
  Programming ability has almost nothing to do with
  the ability to get a fox, a chicken, and a sack of grain across a river
  without anything getting eaten,
  or how to determine which of three switches corresponded to which of three
  light bulbs in an opaque box that can only be opened once on a rainy Tuesday.

- Pumping the ego of the interviewer by asking questions that remind them
  how much smarter they are than the candidate

It's about getting to know each other enough to make an informed hiring decision.
Asking about previous projects and open-ended questions with no single answer
are some of the best ways to do this.

## Don't waste people's time

Running somewhat counter to this is the importance of not wasting an inordinate
amount of everybody's time.
If you know nothing about a candidate,
it's not a bad idea to have a phone or Skype screen to see if an on-site
interview is worthwhile.
Here is where "beginner" questions
are useful --- they're worth the ten minutes they take to ensure the candidate
can code their way out of wet paper bag.
The goal isn't to turn the interview into a trivia show about what they remember
from algorithms class, so keep it simple.
Examples include:

- FizzBuzz

- Reversing a string

- Writing a function that indicates if a string is a valid IPv4 address
  (offer to explain what makes a valid IPv4 if the candidate doesn't know)

- Finding the maximum value in a list of integers.

- Implementing a stack with push, pop, and peek operations.

In my experience these simple weed-out questions eliminate a surprising
percentage of applicants.
Some might complain that they have little to do with day to day
software engineering or are insulting to a seasoned developer,
but anybody who knows their stuff will blow through them,
leaving the rest of your time together for chatting about
past projects and checking schedules to set up the "real" interview.
Those who struggle or take a long time are unlikely to suddenly rise
to the occasion when confronted with actual problems you're paid to solve.
And if you're talking to a developer with a reputation and a widely-used project,
perhaps it makes sense to skip this stuff,
but if all you know about them is what their résumé tells you,
a quick smoke test is a courtesy to everyone involved.
