---
layout: post
title: "Introducing promptd: A few small tools for your shell prompt"
---

If you use any command line tools, your shell and prompt are important.
For most developers, it's something we stare at for a good part of every working day.
And since I'm staring at it constantly, I want a prompt that gives me useful information
without coming off as too busy or cluttered.
Finding nothing that matched quite what I wanted, I wrote one.

## promptd

promptd is a very small set of tools for building your shell prompt.
It currently has two parts:

- `promptd-path`, a path shortener inspired by [fish](http://fishshell.com/),
  prints a shortened version of your current directory.
  You can also have it print the full directory up to a given character limit,
  and shorten anything over.

- `promptd-vcs`, the meat of the project.
  Inspired by [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh#readme),
  this tool uses glyphs to give you a quick overview of your Git repository's status.
  It has the following advantages over the existing oh-my-zsh solution and
  its underlying [vcs_info](http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#Gathering-information-from-version-control-systems)
  plugin:

    - It searches for the name of your current branch in more places,
      including packed refs.

    - `vcs_info` waits for `git status` to complete before displaying your prompt.
      This can take several seconds for a large repository,
      especially the first time you visit it before it can be cached by your OS.
      This creates awkward pauses that are [unacceptable](https://www.youtube.com/watch?v=07So_lJQyqw)
      for something as fundamental as your shell prompt.
      `promptd_vcs` interacts with `git status` using asynchronous I/O.
      This way, you can set limits on its time to run, and if it exceeds them,
      it will stop and use whatever it got so far to generate your prompt.

Together, you can use them to build a prompt like this:

![promptd demo](https://camo.githubusercontent.com/6f20697cb7fb0ecdb18fe860eafefbd50d76c957/687474703a2f2f692e696d6775722e636f6d2f3278686f4975732e676966)

Additional features, such as displaying info when merging and supporting
other VCSes such as Subversion and Mercurial are in the works.

## Nothing revolutionary

![This is the sound of angry men](https://assets.bitbashing.io/images/miserables.jpg)

Nothing here is going to change the world.
This is a tiny little project with more modest goals --- "neat" or "useful" will suffice.
I wrote it over a few days to scratch an itch and have a bit of fun.

If you want to try it out, `promptd` is available [here on Github](https://github.com/mrkline/promptd#readme).
Binary releases for 64-bit Linux are provided, and building it is as easy as running `make`.
Besides a [D complier](http://dlang.org/download.html) to build it, it has no dependencies.

---

**Addendum:** Because several pointed out it sounds too much like a daemon,
`promptd` has been renamed to [`promptoglyph`](https://github.com/mrkline/promptoglyph#readme).
Cheesy, but according to a quick Google search, unique.
