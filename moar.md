---
layout: page
title: PDF Drawer
---

Other things I've written:

[<i>Modern {% include latex.html %}</i>](https://assets.bitbashing.io/modern-latex.pdf),
a (very!) short book on learning {% include latex.html %} without getting wrapped
up in 1980s tech.  
Sources at <https://github.com/mrkline/modern-latex>

[_What every systems programmer should know about concurrency_](https://assets.bitbashing.io/papers/concurrency-primer.pdf),
a brief primer on the topic.
Sources can be found on [Github](https://github.com/mrkline/concurrency-primer)
and [Gitlab](https://gitlab.com/mrkline/concurrency-primer).

[_Comparing Floating-Point Numbers is Tricky_](https://assets.bitbashing.io/papers/floats.pdf)
(Post version [here](/comparing-floats.html),
{% include latex.html %} sources [here](https://assets.bitbashing.io/papers/floats.tar.xz))

[_C++ on Embedded Systems_](https://assets.bitbashing.io/papers/embedded-cpp.pdf)
(Post version [here](/embedded-cpp.html),
{% include latex.html %} source [here](https://assets.bitbashing.io/papers/embedded-cpp.tex))

[_<span class="tex">T<sub>e</sub>X</span>: A tale of two worlds_](https://assets.bitbashing.io/papers/tex-tale-of-two-worlds.pdf)
(Post version [here](/tex.html))

And always remember,  
[_For a successful technology, reality must take precedence over public relations,
for nature cannot be fooled._](https://assets.bitbashing.io/appendixF.pdf)
