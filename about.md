---
layout: page
title: About the author
---

I'm Matt Kline.
I work as a software engineer for [Anduril](https://www.anduril.com/).
Before that, I got my undergrad from the University of Wisconsin-Madison with
degrees in computer engineering and computer sciences.
I like systems programming,
dabbling in video production,
and spending far too much time playing [combat flight sims](https://www.youtube.com/watch?v=S8ovt9ux7v0).

All views expressed here are my own and do not represent the opinion of my cat,
my employers of any tense, or any innocent bystanders.

All posts are licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
