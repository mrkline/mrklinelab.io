---
layout: post
title: My love–hate relationship with Boost
---

## I love Boost.

For those unfamiliar with it, [Boost](http://www.boost.org/) is a fantastic
set of C++ libraries that supplement the somewhat... meager C++ standard library.
It has a well-deserved reputation of excellent quality---so good, in fact, that
several of its libraries have made their way into recent language standards
with barely any modification. Examples include
[smart pointers](http://www.boost.org/doc/libs/release/libs/smart_ptr/smart_ptr.htm),
[regular expressions](http://www.boost.org/doc/libs/release/libs/regex/doc/html/index.html),
and
[hash tables](http://www.boost.org/doc/libs/release/doc/html/unordered.html).
Whenever I end up using them, they solve my problems elegantly.
Boost is great, except...

## I hate Boost.

The crappy thing about Boost is that its helpfulness
comes with a big heap of annoyances.
None of them are showstoppers, but they add up enough to make me always think
twice before adding Boost as a dependency.

### CVBG Boost

Using Boost to solve a problem often feels like bringing a carrier battlegroup
for a weekend fishing trip.

<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/US_Navy_101210-N-6720T-107_SS_George_Washington_(CVN_73)_Carrier_Strike_Group_and_Japan_Maritime_Self-Defense_Force_(JMSDF)_ships_underway_in_forma.jpg/1280px-US_Navy_101210-N-6720T-107_SS_George_Washington_(CVN_73)_Carrier_Strike_Group_and_Japan_Maritime_Self-Defense_Force_(JMSDF)_ships_underway_in_forma.jpg"
     alt="A carrier battlegroup"
     height="400" >
<figcaption>Pictured: <code>boost::fishing_boat</code> and related classes</figcaption>
</figure>

Want an example? Here's Hello World:

```c++
#include <cstdio>

int main()
{
    printf("Hello, world!\n");
    return 0;
}
```

Let's see how much code the headers pull in.

```
$ g++ -std=c++14 -E hi.cpp | wc -l
1052
```

That's roughly a thousand lines from `cstdio` and friends.
Let's check compile time:

```
$ time g++ -std=c++14 -O2 hi.cpp
g++ -std=c++14 -O2 hi.cpp  0.03s user 0.00s system 87% cpu 0.038 total
```

Great. Now let's say I'm doing some work with floats.
I know that [only a fool would compare two floats after doing arithmetic](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/),
so I look around and find that Boost has a
[great set of utilities](http://www.boost.org/doc/libs/release/libs/math/doc/html/utils.html)
to help me out.
Let's modify our Hello World to call one of those functions:

```C++
#include <boost/math/special_functions/next.hpp>

int main()
{
    printf("Hello, world! The distance between 41 and 42 is %d\n",
           (int)boost::math::float_distance(41.0f, 42.0f));
    return 0;
}
```

Look at the amount of code we build now:

```
$ g++ -std=c++14 -E boost.cpp | wc -l
125376
```

Boost libraries are known to depend heavily on each other, and we see a perfect
example here.
Including this _single_ Boost file pulls in several hundred headers
and over 125,000 lines of code.
And because C++ inherits C's practice of, "literally copy-paste everything you
`#include` into a file and compile that",
our build time gets punched in the face.

```
time g++ -std=c++14 -O2 boost.cpp
g++ -std=c++14 -O2 boost.cpp  1.44s user 0.04s system 99% cpu 1.496 total
```

<img src="https://i.imgur.com/yRO9z8n.jpg" height=400" alt="Mother of God.">

Yes, this is just a toy example
(one would hope Hello World compiles faster than a library),
but try it yourself on some "real" code.
The slowdown is noticeable when introducing Boost libraries to other existing,
non-trivial projects.
Now compile time isn't everything (certainly not as important as run time).
But if you're busy debugging in a tweak → compile → test loop,
such a major speed bump is aggravating, and you pay it on a per file basis.
Sure, you can use [precompiled headers](https://en.wikipedia.org/wiki/Precompiled_header)
to treat the symptoms, but something smells foul when you have to modify your
entire build procedure for even a small part of a library.

### A template for madness

Further hampering both build times and your general sanity is the extensive
use of templates in Boost.
Templates themselves aren't evil, but perhaps a bit... abused.
The C++ template system was originally designed for the stuff we all know
and love, [parametric polymorphism](https://en.wikipedia.org/wiki/Parametric_polymorphism).
Now you could use the same code to create both a list of integers and a list of
strings! [Neat](http://www.boost.org/doc/libs/release/libs/geometry/doc/html/geometry/design.html).
But then someone realized templates were Turing complete and shit got weird.
Templates are a double-edge sword. On one hand, their abilities
allow you to do all sorts of fantastic things!
You can precalculate values,
[perform dimensional analysis](http://www.boost.org/doc/libs/release/libs/mpl/doc/tutorial/dimensional-analysis.html),
use classes as tags to specify options at compile-time,
and do so much more, all with no runtime performance hit.
You can even [embed domain specific languages](http://www.boost.org/doc/libs/release/libs/spirit/doc/html/spirit/introduction.html)
straight into your code.
But all this power comes at a price. When you start using templates this way
(which Boost does extensively), issues start hanging over you like the Sword of
Damocles:

<figure>
<pre><code>
In file included from myPoorCode.cpp:28:
In file included from myPoorCode_impl.hpp:31:
In file included from sendHelp.hpp:31:
In file included from somewhere/boost/spirit/include/qi.hpp:16:
In file included from somewhere/boost/spirit/home/qi.hpp:14:
In file included from somewhere/boost/spirit/home/qi/action.hpp:14:
In file included from somewhere/boost/spirit/home/qi/action/action.hpp:14:
In file included from somewhere/boost/spirit/home/qi/meta_compiler.hpp:14:
In file included from somewhere/boost/spirit/home/support/meta_compiler.hpp:16:
In file included from somewhere/boost/spirit/include/phoenix_limits.hpp:11:
In file included from somewhere/boost/phoenix/core/limits.hpp:26:
In file included from somewhere/boost/proto/proto_fwd.hpp:28:
/god/why/include/boost/utility/result_of.hpp:189:46: error: too few
      template arguments for class template 'result'
struct result_of_nested_result : F::template result&lt;FArgs&gt;
                                             ^
/oh/my/boost/utility/result_of.hpp:194:5: note: in
      instantiation of template class 'boost::detail::result_of_nested_result&lt;const somePoorSod, const save::me
      (ack &, double &, double &, double &)&gt;' requested here
  : mpl::if_&lt;is_function_with_no_args&lt;FArgs&gt;,
</code></pre>
<figcaption>Honey, the templates are at it again.</figcaption>
</figure>
<br>

Originally I was resigned to the assumption that this is the heavy price to
pay for metaprogramming and compile-time execution, but I've since tried
other languages that offer them without the pain.
Andrei Alexandrescu [says it best](https://www.reddit.com/r/programming/comments/3cjr4v/generic_programming_must_go/csx2isp?context=3):

> C++ is condemned to only have power for the most modest metaprogramming
> undertakings, and invariably with hugely disproportionate effort.
> The C++ metaprogrammer is in a chronic crisis of balancing usability with
> implementation complexity with syntactic explosion; in doing so,
> inevitably dealing with the complicated implementation mechanics soon take
> front stage and become focal to a disproportionate extent.
>
> That makes your typical C++ metaprogramming article or talk a long and arduous
> arraying of tricks that commingle into glossolalia before long,
> invariably for a rather trivial task.

### Sins of the father

It's fair to note that most of these problems are systemic of C++ itself.
It's not Boost's fault that the template system is quick to flip out or that
its compilation model can be slow.
But in its quest to do all the different and amazing things it does,
Boost runs straight into the nastier bits of C++ at full speed.
When the trouble leaks out onto the user, pain abounds.

But besides all that, Boost is great. Go use it.

----
