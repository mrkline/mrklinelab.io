---
layout: post
title: "Stating the Obvious: Invariants"
---

<!--
I'm in a quandary - on one hand fundamentals seem important to explain.
On the other hand they're, by definition, pretty anodyne.
There's much more interesting stuff to write about (and hopefully, to read).
This will probably be my last of these for a while, sorry.
-->

In our [last episode](./stating-the-obvious-error-handling.html)
on software engineering fundamentals, we discussed error handling.
The basic premise is simple: as our code runs,
it's important to check the assumptions it makes,
and to do something when those assumptions don't hold.
Sometimes that's just telling the caller, "I couldn't open that file",
but other times, errors are _unrecoverable_.
If our code asks for item 9001 in a ten-item list we should BURN IT WITH FIRE,
not cross our fingers and hope stranger things don't happen next.
There's nothing useful left to do, and we should give up before we
[delete our users' hard drives](https://github.com/valvesoftware/steam-for-linux/issues/3671)
or [blow up $370 million rockets](https://en.wikipedia.org/wiki/Ariane_flight_V88).

Along with avoiding total disaster, unrecoverable errors help us another
way: they reduce the number of edge cases we have to keep rattling around
in our squishy meat heads.
Humans are actually pretty awful at tracking lots of things at once,[^1]
so it's vital to break work into chunks we can reason about
*locally*---in isolation from the rest of the code.
The obvious implication is that small functions with simple logic
are better[^2] than big ones with convoluted logic.
But this also forms the basis of classic advice like, "avoid global variables".
It's not good advice because the computer has any problem working with them,
it's good advice because any code that touches a global influences
_all the other code_ that also touches the global.
Without serious discipline, this grows and grows,
and suddenly you have a system you're afraid to change because you'll probably
forget some detail and break it.

## What else can we do?

Besides writing small functions and avoiding global variables, we can:

- **Assert more invariants.** It's not just for errors!
  Validating data _as it enters_ your program
  (from files, network sockets, user input, etc.) means all the other code
  won't have to deal with edge cases throughout.
  Making data immutable eliminates entire classes of problems
  that start with, "What if it changes to..."

- **Don't program by side-effect.** Write functions that take inputs and produce
  outputs without changing variables elsewhere in the program.
  Yes, this is just another way of saying, "don't use global variables",
  but people often miss the connection.

- **Make invalid states unrepresentable.**
  [Listen to Yaron.](https://youtu.be/-J8YyfrSwTk?t=1080)
  Leverage your language's type system however you can so that bogus values
  _can't be expressed_. [Sum types](/std-visit.html) are a helpful ingredient.

Like last time, this is hilariously generic advice, but that's the point.
Details vary from language to language, and some tools are better than others,
but these are good goals for correct, maintainable code.

------

[^1]: Obviously the upper limit varies from person to person,
      but some of the nastiest code I've seen was written by people with a
      superhuman ability to track many things at once.
      Or maybe they were just more familiar because they wrote the thing.
      Either way, their code was completely unmaintainable by the rest of
      the team.


[^2]: You can certainly break code into pieces that are _too small_, as well,
      and suddenly you have to jump between ten functions just to get a basic idea
      of what's going on. To see this taken to hilarious extremes for job security
      purposes, see [_Refuctoring_](https://www.waterfall2006.com/Refuctoring.pdf).
